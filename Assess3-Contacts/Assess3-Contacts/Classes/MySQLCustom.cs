﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

using System.Data;
using MySql.Data.Common;
using MySql.Data.Types;

namespace MySQLDemo.Classes
{
    public class MySQLCustom
    {
        //CHANGE THESE TO MATCH YOUR DB
        private static string host = "localhost";
        private static string user = "root";
        private static string pass = "root";
        private static string dbname = "gui_comp6001_16b_assn3";

        //DO NOT CHANGE THESE
        private static MySqlConnection Connection(string h, string u, string p, string n)
        {
            string serverConnection = String.Format($"Server={h};Database={n};Uid={u};Pwd={p};SslMode=None;charset=utf8");

            MySqlConnection connection = new MySqlConnection(serverConnection);

            return connection;
        }

        public static MySqlConnection conn()
        {
            var a = Connection(host, user, pass, dbname);
            return a;
        }

        public class Results
        {
            public string Row { get; set; }
        }

        public static List<Results> SelectCommand(string command, MySqlConnection connection)
        {
            //Fixes Encoding - Courtsey of Stackoverflow :-)
            EncodingProvider p;
            p = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(p);

            List<Results> listTables = new List<Results>();

            using (connection)
            {
                connection.Open();

                MySqlCommand getCommand = connection.CreateCommand();
                getCommand.CommandText = command;
                using (MySqlDataReader reader = getCommand.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            listTables.Add(new Results() { Row = $"{reader.GetValue(i)}"});
                        }
                    }
                }
            }

            return listTables;
        }

        public static List<string> ShowInList(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return dataList;
        }

        public static string ShowInString(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return string.Join(",", dataList);
        }

      
        //////////////////    DB CLASS ///////////////////////////

        //CHANGE THIS!!
        //This class is specific to your DB
        public class AddPerson
        {
            public string fname { get; set; }
            public string lname { get; set; }
            public string streetNum { get; set; }
            public string streetName { get; set; }
            public string city { get; set; }
            public string postcode { get; set; }
            public string num1 { get; set; }
            public string num2 { get; set; }
            public string email { get; set; }

            public DateTime dob { get; set; }
            public int id { get; set; }
            public AddPerson(string _fname, string _lname, DateTime _dob, string _streetNum, string _streetName, string _city, string _postcode, string _num1, string _num2, string _email)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                streetNum = _streetNum;
                streetName = _streetName;
                city = _city;
                postcode = _postcode;
                num1 = _num1;
                num2 = _num2;
                email = _email;
                
            }

            public AddPerson(int _id)
            {
                id = _id;
            }

            public AddPerson(string _fname, string _lname, DateTime _dob, int _id, string _streetNum, string _streetName, string _city, string _postcode, string _num1, string _num2, string _email)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                id = _id;
                streetNum = _streetNum;
                streetName = _streetName;
                city = _city;
                postcode = _postcode;
                num1 = _num1;
                num2 = _num2;
                email = _email;
            }
        }

        ////////////////////  INSERT STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void AddData(string fname, string lname, string dob, string streetNum, string streetName, string city, string postcode, string num1, string num2, string email)
        {
            var date = DateTime.Parse(dob);

            var customdb = new AddPerson(fname, lname, date, streetName, streetNum, city, postcode, num1, num2, email);

            var con = conn();
            con.Open();

            MySqlCommand insertCommand = con.CreateCommand();

            insertCommand.CommandText = "INSERT INTO tbl_people(FNAME, LNAME, DOB, Str_NUMBER, Str_NAME, CITY, POSTCODE, PHONE1, PHONE2, EMAIL)VALUES(@fname, @lname, @dob, @streetNum, @streetName, @city, @postcode, @num1, @num2, @email)";
            insertCommand.Parameters.AddWithValue("@fname", customdb.fname);
            insertCommand.Parameters.AddWithValue("@lname", customdb.lname);
            insertCommand.Parameters.AddWithValue("@dob", customdb.dob);
            insertCommand.Parameters.AddWithValue("@streetNum", customdb.streetNum);
            insertCommand.Parameters.AddWithValue("@streetName", customdb.streetName);
            insertCommand.Parameters.AddWithValue("@city", customdb.city);
            insertCommand.Parameters.AddWithValue("@postcode", customdb.postcode);
            insertCommand.Parameters.AddWithValue("@num1", customdb.num1);
            insertCommand.Parameters.AddWithValue("@num2", customdb.num2);
            insertCommand.Parameters.AddWithValue("@email", customdb.email);

            insertCommand.ExecuteNonQuery();

            con.Clone();
        }



        ////////////////////  UPDATE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void UpdateData(string fname, string lname, string dob, string id, string streetNum, string streetName, string city, string postcode, string num1, string num2, string email)
        {
            var uid = int.Parse(id);
            var date = DateTime.Parse(dob);

            var customdb = new AddPerson(fname, lname, date, uid, streetName, streetNum, city, postcode, num1, num2, email);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //UPDATE TABLE-NAME SET COLUMN = VALUE WHERE COLUMN = VALUE

            updateCommand.CommandText = "UPDATE tbl_people SET FNAME = @fname, LNAME = @lname, DOB = @dob, str_NUMBER = @streetNum, Str_NAME = @streetName, POSTCODE = @postcode, CITY = @city, PHONE1 = @num1, PHONE2 = @num2, EMAIL = @email WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@fname", customdb.fname);
            updateCommand.Parameters.AddWithValue("@lname", customdb.lname);
            updateCommand.Parameters.AddWithValue("@dob", customdb.dob);
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.Parameters.AddWithValue("@streetNum", customdb.streetNum);
            updateCommand.Parameters.AddWithValue("@streetName", customdb.streetName);
            updateCommand.Parameters.AddWithValue("@city", customdb.city);
            updateCommand.Parameters.AddWithValue("@postcode", customdb.postcode);
            updateCommand.Parameters.AddWithValue("@num1", customdb.num1);
            updateCommand.Parameters.AddWithValue("@num2", customdb.num2);
            updateCommand.Parameters.AddWithValue("@email", customdb.email);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

        ////////////////////  DELETE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void DeleteDate(string id)
        {
            var uid = int.Parse(id);

            var customdb = new AddPerson(uid);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //DELETE FROM TABLE-NAME WHERE COLUM = VALUE

            updateCommand.CommandText = "DELETE FROM tbl_people WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }
    }
}
