﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using MySql.Data.MySqlClient;
using MySQLDemo.Classes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assess3_Contacts
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }


        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}' ";
            var b = MySQLCustom.ShowInList(command);

            userAccount.Text = b[0];
            fnameInput.Text = b[1];
            lnameInput.Text = b[2];
            dobInput.Text = b[3];
            streetNumInput.Text = b[4];
            streetNameInput.Text = b[5];
            cityInput.Text = b[6];
            postcodeInput.Text = b[7];
            num1Input.Text = b[8];
            num2Input.Text = b[9];
            emailInput.Text = b[10];
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            splitView.IsPaneOpen = !splitView.IsPaneOpen;
        }


        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Checks if the object still exists
            if (names.SelectedItem != null)
            {
                selectedName(names.SelectedItem.ToString());
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
           
        }

        void Clear()
        {
            fnameInput.Text = "";
            lnameInput.Text = "";
            dobInput.Text = "";
            streetNameInput.Text = "";
            streetNumInput.Text = "";
            postcodeInput.Text = "";
            cityInput.Text = "";
            num1Input.Text = "";
            num2Input.Text = "";
            emailInput.Text = "";
        }

        static List<string> loadAllTheData()
        {
            //Set the command and executes it and returns a list
            var command = $"Select FNAME from tbl_people";

            //Call the custom method
            var list = MySQLCustom.ShowInList(command);

            //Display the list (in this case a combo list)
            return list;
        }

        private void addToDB_Click(object sender, RoutedEventArgs e)
        {
            //Add Information to the Database
            MySQLCustom.AddData(fnameInput.Text, lnameInput.Text, dobInput.Text, streetNameInput.Text, streetNumInput.Text, cityInput.Text, postcodeInput.Text, num1Input.Text, num2Input.Text, emailInput.Text);
            names.ItemsSource = loadAllTheData();
            Clear();

        }

        private void updateDB_Click(object sender, RoutedEventArgs e)
        {
            //Update Information in the Database
            MySQLCustom.UpdateData(fnameInput.Text, lnameInput.Text, dobInput.Text, streetNameInput.Text, streetNumInput.Text, cityInput.Text, postcodeInput.Text, num1Input.Text, num2Input.Text, emailInput.Text, userAccount.Text);
            names.ItemsSource = loadAllTheData();
            Clear();
        }

        private void removeFromDB_Click(object sender, RoutedEventArgs e)
        {
            //Remove Information from the Database
            MySQLCustom.DeleteDate(userAccount.Text);
            names.ItemsSource = loadAllTheData();
            Clear();
        }

        private void autoFill_Click(object sender, RoutedEventArgs e)
        {
            fnameInput.Text = "Luke";
            lnameInput.Text = "Johnston";
            dobInput.Text = "11/17/1992";
            streetNameInput.Text = "Kakapo Place";
            streetNumInput.Text = "5";
            postcodeInput.Text = "3112";
            cityInput.Text = "Tauranga";
            num1Input.Text = "0220754198";
            num2Input.Text = "075435272";
            emailInput.Text = "lukejohnston@hotmail.co.nz";
        }
    }
}
