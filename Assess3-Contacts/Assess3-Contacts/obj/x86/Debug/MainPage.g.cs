﻿#pragma checksum "C:\Users\Luke\Desktop\gui-ass3\gui-assessment3-9954067\Assess3-Contacts\Assess3-Contacts\MainPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2732692A12A7E885A17A6502FAB4D481"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Assess3_Contacts
{
    partial class MainPage : 
        global::Windows.UI.Xaml.Controls.Page, 
        global::Windows.UI.Xaml.Markup.IComponentConnector,
        global::Windows.UI.Xaml.Markup.IComponentConnector2
    {
        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 1:
                {
                    this.MainGrid = (global::Windows.UI.Xaml.Controls.Grid)(target);
                }
                break;
            case 2:
                {
                    this.splitView = (global::Windows.UI.Xaml.Controls.SplitView)(target);
                }
                break;
            case 3:
                {
                    this.SecondGrid = (global::Windows.UI.Xaml.Controls.Grid)(target);
                }
                break;
            case 4:
                {
                    this.ThirdGrid = (global::Windows.UI.Xaml.Controls.Grid)(target);
                }
                break;
            case 5:
                {
                    this.MainHeading = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 6:
                {
                    this.fname = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 7:
                {
                    this.lname = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 8:
                {
                    this.dob = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 9:
                {
                    this.streetNum = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 10:
                {
                    this.streetName = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 11:
                {
                    this.postcode = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 12:
                {
                    this.num1 = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 13:
                {
                    this.num2 = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 14:
                {
                    this.email = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 15:
                {
                    this.city = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 16:
                {
                    this.addToDB = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 85 "..\..\..\MainPage.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.addToDB).Click += this.addToDB_Click;
                    #line default
                }
                break;
            case 17:
                {
                    this.updateDB = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 92 "..\..\..\MainPage.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.updateDB).Click += this.updateDB_Click;
                    #line default
                }
                break;
            case 18:
                {
                    this.removeFromDB = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 99 "..\..\..\MainPage.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.removeFromDB).Click += this.removeFromDB_Click;
                    #line default
                }
                break;
            case 19:
                {
                    this.btnClear = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 106 "..\..\..\MainPage.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.btnClear).Click += this.btnClear_Click;
                    #line default
                }
                break;
            case 20:
                {
                    this.fnameInput = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 21:
                {
                    this.lnameInput = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 22:
                {
                    this.dobInput = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 23:
                {
                    this.streetNumInput = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 24:
                {
                    this.streetNameInput = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 25:
                {
                    this.postcodeInput = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 26:
                {
                    this.num1Input = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 27:
                {
                    this.num2Input = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 28:
                {
                    this.cityInput = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 29:
                {
                    this.emailInput = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 30:
                {
                    this.userAccount = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 31:
                {
                    this.autoFill = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 121 "..\..\..\MainPage.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.autoFill).Click += this.autoFill_Click;
                    #line default
                }
                break;
            case 32:
                {
                    this.extendPanel = (global::Windows.UI.Xaml.Controls.StackPanel)(target);
                }
                break;
            case 33:
                {
                    this.HamburgerButton = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 31 "..\..\..\MainPage.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.HamburgerButton).Click += this.HamburgerButton_Click;
                    #line default
                }
                break;
            case 34:
                {
                    this.contactHeader = (global::Windows.UI.Xaml.Controls.PivotItem)(target);
                }
                break;
            case 35:
                {
                    this.infoHeader = (global::Windows.UI.Xaml.Controls.PivotItem)(target);
                }
                break;
            case 36:
                {
                    this.information = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 37:
                {
                    this.names = (global::Windows.UI.Xaml.Controls.ListView)(target);
                    #line 45 "..\..\..\MainPage.xaml"
                    ((global::Windows.UI.Xaml.Controls.ListView)this.names).SelectionChanged += this.names_SelectionChanged;
                    #line default
                }
                break;
            case 38:
                {
                    this.FirstGrid = (global::Windows.UI.Xaml.Controls.Grid)(target);
                }
                break;
            case 39:
                {
                    this.Monitor = (global::Windows.UI.Xaml.VisualState)(target);
                }
                break;
            case 40:
                {
                    this.Phone = (global::Windows.UI.Xaml.VisualState)(target);
                }
                break;
            case 41:
                {
                    this.Tablet_13 = (global::Windows.UI.Xaml.VisualState)(target);
                }
                break;
            case 42:
                {
                    this.Tablet_8 = (global::Windows.UI.Xaml.VisualState)(target);
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Windows.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Windows.UI.Xaml.Markup.IComponentConnector returnValue = null;
            return returnValue;
        }
    }
}

